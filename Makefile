CC=gcc
LIBS=-lnfc -lusb -lfreefare
LDFLAGS=-L/opt/local/lib
CPPFLAGS=-I/usr/local/include
all: 
	$(CC) freefare-test.c -std=c99 -g -O2 -o freefare-test $(LIBS) $(LDFLAGS) $(CPPFLAGS)
clean:
	rm freefare-test
