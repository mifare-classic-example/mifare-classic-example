/*
 *
 * MiFare Classic Demonstration using libfreefare and libnfc
 *
 * (C) 2012 Mathew McBride 
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */


#include <nfc/nfc.h>
#include <freefare.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
static nfc_device *device = NULL;
static MifareTag *tags = NULL;
MifareTag tag = NULL;

MifareClassicKey k = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };
MifareClassicKey b = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
MifareClassicBlock testData = {0x00,0xDE,0xAD,0x00,0x00,0xBE,0xEF,0x00,0x00,0xCA,0xFE,0x00,0x00,0xBA,0xBE,0x00};
MifareClassicBlock blankTrailer = {0x00,0x00,0x00,0x00,0x000,0x00,0xff,0x07,0x80,0x69,0x00,0x00,0x00,0x00,0x00,0x00};
MifareClassicKey customSectorTwoKey = "hacker";

//#define SECTOR_TWO_HAS_CUSTOM_KEY 
//#define SET_SECTOR_TWO_TO_CUSTOM_KEY 1 
//#define RESET_SECTOR_TWO_KEY 

void print_hex(unsigned char * pbtData, const size_t szBytes);

int main(int argc, char **argv) 
{
    int res;
    nfc_connstring devices[8];
    size_t device_count;
    
    nfc_init(NULL);

	char *uid;
    device_count = nfc_list_devices (NULL, devices, 8);
    if (device_count <= 0) {
	printf("No NFC device found\n");
	return 1;
    }
	
    MifareClassicBlock data;
	
    for (size_t i = 0; i < device_count; i++) {
        device = nfc_open (NULL, devices[i]);
        if (!device)  {
		printf("Could not open NFC device\n");
		return 1;
	}

        tags = freefare_get_tags (device);
	if (tags == NULL) {
		printf("No tags on reader!\n");
		return 1;
	}
        tag = NULL;
        for (int i=0; tags[i]; i++) {
            if ((freefare_get_tag_type(tags[i]) == CLASSIC_1K) ||
                (freefare_get_tag_type(tags[i]) == CLASSIC_4K)) {
                tag = tags[i];
                res = mifare_classic_connect (tag);
		if (res != 0) {
			printf("Could not connect to the MiFare Classic\n");	
			return 1;
		}
		uid = freefare_get_tag_uid(tag);
		printf("Have mifare UID: %s\n",uid);
		MifareClassicBlockNumber block;
		for(block=0; block<63; block++) {
			memset(&data,0,16);
			#ifdef SECTOR_TWO_HAS_CUSTOM_KEY 
			if (block >= 4 && block <= 7) {
				res = mifare_classic_authenticate(tag,block,customSectorTwoKey,MFC_KEY_A);	
				printf("%d\n",res);
			} else {
			#endif
			res = mifare_classic_authenticate(tag,block,k,MFC_KEY_A);
			#ifdef SECTOR_TWO_HAS_CUSTOM_KEY
			}
			#endif
			if (res != 0) {
				printf("Could not authenticate to block %d\n",block);
			}
			res = mifare_classic_read(tag,block,&data);
			if (res != 0) {
				printf("Could not read block %d",block);
				return 1;
			}
			printf("%2d: ",block);
			print_hex(&data[0],16);
		}
		res = mifare_classic_authenticate(tag,1,k,MFC_KEY_A);
		if (res == 0) {
			res = mifare_classic_write(tag,1,testData);
			if (res != 0) {
				printf("Writing data to block 1 failed\n");
			}
		}
		#ifdef SET_SECTOR_TWO_TO_CUSTOM_KEY
		res = mifare_classic_authenticate(tag,7,k,MFC_KEY_A);
		if (res == 0) {
			res = mifare_classic_read(tag,7,&data);
			memcpy(&data[0],customSectorTwoKey,6);
			printf("About to set sector trailer: ");
			print_hex(&data[0],16);
			res = mifare_classic_write(tag,7,data);
			if (res != 0) {
				printf("Failed to write sector trailer for sector 2\n");
			}
		} else {
		   printf("Could not authenticate to sector two using original key.. is the key already set?\n");
		} 
		#endif

		#ifdef RESET_SECTOR_TWO_KEY

		MifareClassicBlockNumber hSectorTrailer = 7;
		mifare_classic_authenticate(tag,hSectorTrailer-1,customSectorTwoKey,MFC_KEY_A);
		res = mifare_classic_authenticate(tag,hSectorTrailer,customSectorTwoKey,MFC_KEY_A);
		//res = mifare_classic_authenticate(tag,hSectorTrailer,b,MFC_KEY_B);
		printf("%d\n",res);
		if (res == 0) {
			memset(&data,0,16);
			printf("Authenticated to sector two using custom key\n");
			// Try to set it back to normal key
			res = mifare_classic_read(tag,hSectorTrailer,&data);
			if (res != 0) {
				printf("Could not read sector trailer\n");
			}
			memcpy(&data[0],blankTrailer,16);
			memcpy(&data[0],k,6);
			printf("About to set sector trailer: ");
			print_hex(&data[0],16);

			res = mifare_classic_write(tag,hSectorTrailer,data);
			if (res != 0) {
				printf("Failed to write sector trailer for sector 2\n");
			}
		} 
		#endif		
            }
        }
        nfc_close (device);
        device = NULL;
        freefare_free_tags (tags);
        tags = NULL;
    }
	return 0;
}
/** Stolen from libnfc */

void print_hex(unsigned char *pbtData, const size_t szBytes)
{
  size_t szPos;

  for (szPos=0; szPos < szBytes; szPos++)
  {
    printf("%02x  ",pbtData[szPos]);
  }
  printf("\n");
}

